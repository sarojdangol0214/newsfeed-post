package com.intern.newsFeedpost.util;

import java.util.Map;

public class Mail {
    private Map< String, Object > model;

    public Map<String, Object> getModel() {
        return model;
    }

    public void setModel(Map<String, Object> model) {
        this.model = model;
    }
}
