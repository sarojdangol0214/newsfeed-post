package com.intern.newsFeedpost.entity.reaction;

import com.intern.newsFeedpost.entity.comment.Comment;
import com.intern.newsFeedpost.entity.newsFeed.NewsFeed;
import com.intern.newsFeedpost.entity.users.Users;

import javax.persistence.*;

@Entity
@Table(name = "reaction")
public class Reaction {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "reaction")
    private String reaction;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users users;

    @OneToOne
    @JoinColumn(name = "newsFeed_id", referencedColumnName = "id")
    private NewsFeed newsFeed;

    @OneToOne
    @JoinColumn(name = "comment_id", referencedColumnName = "id")
    private Comment comment;


}
