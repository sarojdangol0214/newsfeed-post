package com.intern.newsFeedpost.entity.comment;

import com.intern.newsFeedpost.entity.newsFeed.NewsFeed;
import com.intern.newsFeedpost.entity.users.Users;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "comment")
    private String comment;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users users;

    @Column(name = "is_reply")
    private boolean is_reply=false;

    @Column(name = "reply_id")
    private Long replyId;

    @OneToOne
    @JoinColumn(name = "newsFeed_id", referencedColumnName = "id")
    private NewsFeed newsFeed;
}
