package com.intern.newsFeedpost.constant;

public enum UserType {
    ADMIN,
    USER
}
