package com.intern.newsFeedpost.constant;

public enum RegistrationStatus {
    OTP_PENDING,
    ADD_INFO_PENDING,
    SUBMITTED,
    REGISTERED
}
