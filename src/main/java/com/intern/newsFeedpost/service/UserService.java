package com.intern.newsFeedpost.service;

import com.intern.newsFeedpost.dto.user.*;

public interface UserService {
    RegisterResponse registerEmail(RegisterEmailRequest request);

    RegisterResponse resendOtp(ResendOtpRequest request);

    ValidateOtpResponse validateOtp(ValidateOtpRequest request);

    AddInformationResponse<Long> addInformation(AddInformationRequest request);

    LoginResponse login(LoginRequest request);

    UserInformationResponse updateUserInformation(UpdateInformationRequest request);

    UserInformationResponse getUserInformation();
}
