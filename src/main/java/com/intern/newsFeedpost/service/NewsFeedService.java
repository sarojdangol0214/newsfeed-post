package com.intern.newsFeedpost.service;

import com.intern.newsFeedpost.dto.newsFeed.*;

import java.util.List;

public interface NewsFeedService {
    PostNewsFeedResponse postNewsFeed(PostNewsFeedRequest request);

    NewsFeedResponse updateNewsFeed(UpdateNewsFeedRequest request);

    UsersDetailsResponse getUserNewsFeed();

    List<NewsFeedDetailsResponse> getAllNewsFeed();

    DeleteNewsFeedResponse deleteNewsFeed(Long id);

}
