package com.intern.newsFeedpost.service.validator;

import com.intern.newsFeedpost.entity.users.Users;

public interface UserValidator {
    Users validateUserByUserNameForRegistration(String email);

    Users validateUser(Long id, String userType);
}
