package com.intern.newsFeedpost.service;

import com.intern.newsFeedpost.dto.AddCommentResponse;
import com.intern.newsFeedpost.dto.comment.AddCommentRequest;
import com.intern.newsFeedpost.dto.comment.CommentResponse;
import com.intern.newsFeedpost.dto.comment.DeleteCommentResponse;
import com.intern.newsFeedpost.dto.comment.UpdateCommentRequest;

import java.util.List;

public interface CommentService {
    AddCommentResponse addComment(AddCommentRequest request);

    AddCommentResponse updateComment(UpdateCommentRequest request);

    List<CommentResponse> getAllComment();

    DeleteCommentResponse deleteComment(Long id);

}
