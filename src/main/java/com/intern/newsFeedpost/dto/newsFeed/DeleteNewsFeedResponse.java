package com.intern.newsFeedpost.dto.newsFeed;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DeleteNewsFeedResponse {
    private Long newsFeedId;
    private String message;
}
