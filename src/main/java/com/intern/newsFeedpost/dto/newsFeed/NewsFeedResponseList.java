package com.intern.newsFeedpost.dto.newsFeed;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.bytebuddy.implementation.bind.annotation.SuperCall;

import java.util.List;


@Data
public class NewsFeedResponseList{

    private Long messageId;
    private String message;
}
