package com.intern.newsFeedpost.dto.user;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RegisterResponse {
    private final Long id;
    private final String otp;
    private final String username;
    private final String message;
}
