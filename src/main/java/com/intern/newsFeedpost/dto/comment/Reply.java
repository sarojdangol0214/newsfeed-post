package com.intern.newsFeedpost.dto.comment;

import com.intern.newsFeedpost.entity.comment.Comment;
import lombok.Data;

@Data
public class Reply{
    private Long id;
    private String username;
    private String replyMessage;
}
