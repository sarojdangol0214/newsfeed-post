package com.intern.newsFeedpost.dto.comment;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CommentResponse {
    private NewsFeedResponseForComment newsFeedData;
    private CommentData commentData;
}
