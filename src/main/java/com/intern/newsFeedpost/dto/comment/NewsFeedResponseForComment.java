package com.intern.newsFeedpost.dto.comment;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Getter
public class NewsFeedResponseForComment {
    private Long newsFeedId;
    private String newsFeed;
}
