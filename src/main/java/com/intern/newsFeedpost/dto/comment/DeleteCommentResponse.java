package com.intern.newsFeedpost.dto.comment;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DeleteCommentResponse {
    private String message;
}
