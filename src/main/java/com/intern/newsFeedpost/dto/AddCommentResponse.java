package com.intern.newsFeedpost.dto;

import com.intern.newsFeedpost.dto.comment.Reply;
import com.intern.newsFeedpost.entity.comment.Comment;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class AddCommentResponse {
    private String newsFeed;
    private String username;
    private Long commentId;
    private String comment;
    private boolean isReply;

}
