package com.intern.newsFeedpost.commons.context;

import com.intern.newsFeedpost.constant.UserType;
import com.intern.newsFeedpost.entity.users.Users;
import com.intern.newsFeedpost.repository.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class ContextHolderService {

    private UserRepository userRepository;

    @Autowired
    public ContextHolderService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Context getContext() {
        return ContextHolder.get();
    }

    public void setContext(String username, String userType, List<String> permissions) {
        if (userType.equalsIgnoreCase(UserType.USER.name())) {
            this.setContextForClient(username);
        }
    }

    private void setContextForClient(String username) {
        Optional<Users> userOptional = userRepository.findByUsername(username);
        userOptional.ifPresent(user -> {
            ContextHolder thread = new ContextHolder(user.getId(), user.getPhoneNumber(), UserType.USER.name());
            thread.run();
        });
    }


}
