package com.intern.newsFeedpost.controller.comment;

import static com.intern.newsFeedpost.constant.Route.*;

import com.intern.newsFeedpost.dto.AddCommentResponse;
import com.intern.newsFeedpost.dto.comment.AddCommentRequest;
import com.intern.newsFeedpost.dto.comment.CommentResponse;
import com.intern.newsFeedpost.dto.comment.DeleteCommentResponse;
import com.intern.newsFeedpost.dto.comment.UpdateCommentRequest;
import com.intern.newsFeedpost.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    @PostMapping(value = ADD_COMMENT)
    public AddCommentResponse addComment(@Valid @RequestBody AddCommentRequest request) {
        return commentService.addComment(request);
    }

    @PutMapping(value = UPDATE_COMMENT)
    public AddCommentResponse updateComment(@Valid @RequestBody UpdateCommentRequest request) {
        return commentService.updateComment(request);
    }

    @GetMapping(value = GET_COMMENT)
    public List<CommentResponse> getAllComment(){
        return commentService.getAllComment();
    }

    @DeleteMapping(value = DELETE_COMMENT)
    public DeleteCommentResponse deleteComment(@Valid @PathVariable Long id){
        return commentService.deleteComment(id);
    }
}
