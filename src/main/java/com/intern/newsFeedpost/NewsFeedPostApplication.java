package com.intern.newsFeedpost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsFeedPostApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsFeedPostApplication.class, args);
	}

}
